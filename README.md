# Variational Autoencoder (VAE) #

Material to the talk I gave in the Deep Learning Seminar of Prof. Hamprecht at
Heidelberg University and the Machine Learning Meetup in Heidelberg.

### My slides and a short summary of the talk ###
* [Slides](https://bitbucket.org/tomsal/pub/raw/b2a7a891bdfa21f572120ac15a4667c4e67cd9fd/VAE_slides.pdf)  
* [Summary](https://bitbucket.org/tomsal/pub/raw/c761385d3ca4472e724ef9b1c00f4f9056f6bb4d/VAE_summary.pdf)

### A few useful links: ###
* [Original paper](http://arxiv.org/abs/1312.6114)  
* [Deep Convolutional Inverse Graphics Network](https://arxiv.org/abs/1503.03167)  
* [Demo: VAE Morphing faces](http://vdumoulin.github.io/morphing_faces/)  
* [Demo: AE ConvNetJS MNIST](http://cs.stanford.edu/people/karpathy/convnetjs/demo/mnist.html)
* [Blog Post: VAEs in TensorFlow](https://jmetzen.github.io/2015-11-27/vae.html)  
* [Blog Post: Introduction to
VAEs](http://blog.fastforwardlabs.com/post/148842796218/introducing-variational-autoencoders-in-prose-and)